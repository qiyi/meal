package main

import "fmt"

func main() {   
    myMenu, _ := LoadMenu("menu.json")
    fmt.Println(myMenu)
    SaveMenu(myMenu, "another.json")
}