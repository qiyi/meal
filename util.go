package main

import "encoding/json"
import "io/ioutil"

func LoadMenu(file string) (menu map[string][]Dish, err error) {
    fileBytes, err := ioutil.ReadFile(file)
    if err != nil {
        return
    }
    json.Unmarshal(fileBytes, &menu)
    return
}

func SaveMenu(menu map[string][]Dish, file string) error {
    b, err := json.Marshal(menu)
    if err != nil {
        return err
    }
    err =  ioutil.WriteFile(file, b, 0700)
    return err
}